<?php

namespace App\Controller;

use App\Entity\Company;
use App\Form\CompanyType;
use App\Tools\FormHelper;
use App\Tools\Paginator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

class CompanyController extends AbstractController
{
    /**
     * @Route("/companies/{id}", name="company_read", methods={"GET"})
     */
    public function read($id)
    {
        $company = $this->getDoctrine()
            ->getRepository(Company::class)
            ->find($id);

        if (!$company) {
            return $this->json([
                'code'    => 404,
                'message' => 'No company found for id ' . $id
            ], 404);
        }

        return $this->json($company, 200, [], [AbstractNormalizer::IGNORED_ATTRIBUTES => ['employees']]);
    }

    /**
     * @Route("/companies", name="company_create", methods={"POST"})
     */
    public function create(Request $request, FormHelper $formHelper)
    {
        $company = new Company();
        $form    = $this->createForm(CompanyType::class, $company)
            ->submit($request->request->all())
            ->handleRequest($request);

        if (!$form->isValid()) {
            return $this->json(['errors' => $formHelper->getErrorMessages($form)], 400);
        }

        $entityManager = $this->getDoctrine()->getManager();

        $entityManager->persist($company);

        $entityManager->flush();

        return $this->json($company, 200, [], [AbstractNormalizer::IGNORED_ATTRIBUTES => ['employees']]);
    }

    /**
     * @Route("/companies/{id}", name="company_update", methods={"PUT"})
     */
    public function update($id, Request $request, FormHelper $formHelper)
    {
        $company = $this->getDoctrine()
            ->getRepository(Company::class)
            ->find($id);

        if (!$company) {
            return $this->json([
                'code'    => 404,
                'message' => 'No company found for id ' . $id
            ], 404);
        }

        $form = $this->createForm(CompanyType::class, $company)
            ->submit($request->request->all())
            ->handleRequest($request);

        if (!$form->isValid()) {
            return $this->json(['errors' => $formHelper->getErrorMessages($form)], 400);
        }

        $entityManager = $this->getDoctrine()->getManager();

        $entityManager->persist($company);

        $entityManager->flush();

        return $this->json($company, 200, [], [AbstractNormalizer::IGNORED_ATTRIBUTES => ['employees']]);
    }

    /**
     * @Route("/companies/{id}", name="company_delete", methods={"DELETE"})
     */
    public function delete($id)
    {
        $company = $this->getDoctrine()
            ->getRepository(Company::class)
            ->find($id);

        if (!$company) {
            return $this->json([
                'code'    => 404,
                'message' => 'No company found for id ' . $id
            ], 404);
        }

        try {
            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->remove($company);

            $entityManager->flush();
        } catch (\Exception $e) {
            return $this->json([
                'code'    => 500,
                'message' => $e->getMessage()
            ], 500);
        }

        return new Response();
    }

    /**
     * @Route("/companies", name="company_list", methods={"GET"})
     */
    public function list(Request $request, Paginator $paginator)
    {
        $page      = $request->query->get('page') ?: 1;
        $companies = $paginator->findByPage(
            $this->getDoctrine()->getRepository(Company::class),
            $page
        );

        if (!$companies) {
            return $this->json([
                'code'    => 404,
                'message' => 'No companies found for page ' . $page
            ], 404);
        }

        return $this->json($companies, 200, [], [AbstractNormalizer::IGNORED_ATTRIBUTES => ['employees']]);
    }
}
