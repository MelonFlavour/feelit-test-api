<?php

namespace App\Controller;

use App\Entity\Role;
use App\Form\RoleType;
use App\Tools\FormHelper;
use App\Tools\Paginator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

class RoleController extends AbstractController
{
    /**
     * @Route("/roles/{id}", name="role_read", methods={"GET"})
     */
    public function read($id)
    {
        $role = $this->getDoctrine()
            ->getRepository(Role::class)
            ->find($id);

        if (!$role) {
            return $this->json([
                'code'    => 404,
                'message' => 'No role found for id ' . $id
            ], 404);
        }

        return $this->json($role, 200, [], [AbstractNormalizer::IGNORED_ATTRIBUTES => ['employees']]);
    }

    /**
     * @Route("/roles", name="role_create", methods={"POST"})
     */
    public function create(Request $request, FormHelper $formHelper)
    {
        $role = new Role();
        $form = $this->createForm(RoleType::class, $role)
            ->submit($request->request->all())
            ->handleRequest($request);

        if (!$form->isValid()) {
            return $this->json(['errors' => $formHelper->getErrorMessages($form)], 400);
        }

        $entityManager = $this->getDoctrine()->getManager();

        $entityManager->persist($role);

        $entityManager->flush();

        return $this->json($role, 200, [], [AbstractNormalizer::IGNORED_ATTRIBUTES => ['employees']]);
    }

    /**
     * @Route("/roles/{id}", name="role_delete", methods={"DELETE"})
     */
    public function delete($id)
    {
        $role = $this->getDoctrine()
            ->getRepository(Role::class)
            ->find($id);

        if (!$role) {
            return $this->json([
                'code'    => 404,
                'message' => 'No role found for id ' . $id
            ], 404);
        }

        try {
            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->remove($role);

            $entityManager->flush();
        } catch (\Exception $e) {
            return $this->json([
                'code'    => 500,
                'message' => $e->getMessage()
            ], 500);
        }

        return new Response();
    }

    /**
     * @Route("/roles", name="role_list", methods={"GET"})
     */
    public function list(Request $request, Paginator $paginator)
    {
        $page  = $request->query->get('page') ?: 1;
        $roles = $paginator->findByPage(
            $this->getDoctrine()->getRepository(Role::class),
            $page
        );

        if (!$roles) {
            return $this->json([
                'code'    => 404,
                'message' => 'No roles found for page ' . $page
            ], 404);
        }

        return $this->json($roles, 200, [], [AbstractNormalizer::IGNORED_ATTRIBUTES => ['employees']]);
    }
}
