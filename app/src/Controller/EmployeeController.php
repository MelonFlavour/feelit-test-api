<?php

namespace App\Controller;

use App\Entity\Company;
use App\Entity\Employee;
use App\Entity\Role;
use App\Form\EmployeeType;
use App\Message\NewEmployeeEmail;
use App\Tools\FormHelper;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;


class EmployeeController extends AbstractController
{
    /**
     * @Route("/employees/{id}", name="employee_read", methods={"GET"})
     */
    public function read($id)
    {
        $employee = $this->getDoctrine()
            ->getRepository(Employee::class)
            ->find($id);

        if (!$employee) {
            return $this->json([
                'code'    => 404,
                'message' => 'No employee found for id ' . $id
            ], 404);
        }

        return $this->json(
            $employee,
            200,
            [],
            [AbstractNormalizer::ATTRIBUTES => ['id', 'firstName', 'lastName', 'email', 'company' => ['id', 'name'], 'roles' => ['id', 'name']]]
        );
    }

    /**
     * @Route("/companies/{id}/employees", name="employee_create", methods={"POST"})
     */
    public function create($id, Request $request, FormHelper $formHelper)
    {
        /** @var Company $company */
        $company = $this->getDoctrine()
            ->getRepository(Company::class)
            ->find($id);

        if (!$company) {
            return $this->json([
                'code'    => 404,
                'message' => 'No company found for id ' . $id
            ], 404);
        }

        $employee = new Employee();
        $form     = $this->createForm(EmployeeType::class, $employee)
            ->submit($request->request->all())
            ->handleRequest($request);

        if (!$form->isValid()) {
            return $this->json(['errors' => $formHelper->getErrorMessages($form)], 400);
        }

        $employee->setCompany($company);

        $entityManager = $this->getDoctrine()->getManager();

        $entityManager->persist($employee);

        $entityManager->flush();

        $this->dispatchMessage(new NewEmployeeEmail($employee->getId()));

        return $this->json(
            $employee,
            200,
            [],
            [AbstractNormalizer::ATTRIBUTES => ['id', 'firstName', 'lastName', 'email', 'company' => ['id', 'name']]]
        );
    }

    /**
     * @Route("/employees/{id}", name="employee_update", methods={"PUT"})
     */
    public function update($id, Request $request, FormHelper $formHelper)
    {
        $employee = $this->getDoctrine()
            ->getRepository(Employee::class)
            ->find($id);

        if (!$employee) {
            return $this->json([
                'code'    => 404,
                'message' => 'No employee found for id ' . $id
            ], 404);
        }

        $form = $this->createForm(EmployeeType::class, $employee)
            ->submit($request->request->all())
            ->handleRequest($request);

        if (!$form->isValid()) {
            return $this->json(['errors' => $formHelper->getErrorMessages($form)], 400);
        }

        $entityManager = $this->getDoctrine()->getManager();

        $entityManager->persist($employee);

        $entityManager->flush();

        return $this->json(
            $employee,
            200,
            [],
            [AbstractNormalizer::ATTRIBUTES => ['id', 'firstName', 'lastName', 'email', 'company' => ['id', 'name']]]
        );
    }

    /**
     * @Route("/employees/{id}", name="employee_delete", methods={"DELETE"})
     */
    public function delete($id)
    {
        $employee = $this->getDoctrine()
            ->getRepository(Employee::class)
            ->find($id);

        if (!$employee) {
            return $this->json([
                'code'    => 404,
                'message' => 'No employee found for id ' . $id
            ], 404);
        }

        try {
            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->remove($employee);

            $entityManager->flush();
        } catch (\Exception $e) {
            return $this->json([
                'code'    => 500,
                'message' => $e->getMessage()
            ], 500);
        }

        return new Response();
    }

    /**
     * @Route("/companies/{id}/employees", name="employee_list", methods={"GET"})
     */
    public function list($id, Request $request)
    {
        /** @var Company $company */
        $company = $this->getDoctrine()
            ->getRepository(Company::class)
            ->find($id);


        if (!$company) {
            return $this->json([
                'code'    => 404,
                'message' => 'No company found for id ' . $id
            ], 404);
        }

        $page = $request->query->get('page') ?: 1;

        $employees = $this
            ->getDoctrine()
            ->getRepository(Employee::class)
            ->findByPage($company, $page);

        if (!$employees) {
            return $this->json([
                'code'    => 404,
                'message' => sprintf('No employees found for Category ID#%s page#%s ', $id, $page)
            ], 404);
        }

        return $this->json(
            $employees,
            200,
            [],
            [AbstractNormalizer::ATTRIBUTES => ['id', 'firstName', 'lastName', 'email']]
        );
    }


    /**
     * @Route("/employees/{employeeId}/roles/{roleId}", name="employee_role_create", methods={"POST"})
     */
    public function employeeRoleCreate($employeeId, $roleId)
    {
        /** @var Employee $employee */
        $employee = $this->getDoctrine()
            ->getRepository(Employee::class)
            ->find($employeeId);

        if (!$employee) {
            return $this->json([
                'code'    => 404,
                'message' => 'No employee found for id ' . $employeeId
            ], 404);
        }

        /** @var Role $role */
        $role = $this->getDoctrine()
            ->getRepository(Role::class)
            ->find($roleId);

        if (!$role) {
            return $this->json([
                'code'    => 404,
                'message' => 'No role found for id ' . $roleId
            ], 404);
        }

        $exists = $employee->getRoles()->filter(
            function ($entry) use ($role) {
                return $entry->getId() === $role->getId();
            }
        );

        if ($exists->count() > 0) {
            return $this->json([
                'code'    => 409,
                'message' => sprintf('Role ID#%s already assigned to Employee ID#%s', $roleId, $employeeId)
            ], 409);
        }

        $employee->addRole($role);

        $entityManager = $this->getDoctrine()->getManager();

        $entityManager->persist($employee);

        $entityManager->flush();

        return $this->json(
            $employee,
            200,
            [],
            [AbstractNormalizer::ATTRIBUTES => [
                'id',
                'firstName',
                'lastName',
                'email',
                'company' => ['id', 'name'],
                'roles'   => ['id', 'name']
            ]]
        );
    }

    /**
     * @Route("/employees/{id}/roles", name="employee_roles_list", methods={"GET"})
     */
    public function rolesList($id)
    {
        /** @var Employee $employee */
        $employee = $this->getDoctrine()
            ->getRepository(Employee::class)
            ->find($id);

        if (!$employee) {
            return $this->json([
                'code'    => 404,
                'message' => 'No employee found for id ' . $id
            ], 404);
        }

        if ($employee->getRoles()->count() === 0) {
            return $this->json([
                'code'    => 404,
                'message' => sprintf('Employee ID#%s does not have roles assigned', $id)
            ], 404);
        }

        return $this->json(
            $employee->getRoles(),
            200,
            [],
            [AbstractNormalizer::ATTRIBUTES => ['id', 'name']]
        );
    }

    /**
     * @Route("/employees/{employeeId}/roles/{roleId}", name="employee_role_delete", methods={"DELETE"})
     */
    public function employeeRoleDelete($employeeId, $roleId)
    {
        /** @var Employee $employee */
        $employee = $this->getDoctrine()
            ->getRepository(Employee::class)
            ->find($employeeId);

        if (!$employee) {
            return $this->json([
                'code'    => 404,
                'message' => 'No employee found for id ' . $employeeId
            ], 404);
        }

        /** @var Role $role */
        $role = $this->getDoctrine()
            ->getRepository(Role::class)
            ->find($roleId);

        if (!$role) {
            return $this->json([
                'code'    => 404,
                'message' => 'No role found for id ' . $roleId
            ], 404);
        }

        $exists = $employee->getRoles()->filter(
            function ($entry) use ($role) {
                return $entry->getId() === $role->getId();
            }
        );

        if ($exists->count() === 0) {
            return $this->json([
                'code'    => 409,
                'message' => sprintf('Role ID#%s already unassigned from Employee ID#%s', $roleId, $employeeId)
            ], 409);
        }

        $employee->removeRole($role);

        $entityManager = $this->getDoctrine()->getManager();

        $entityManager->persist($employee);

        $entityManager->flush();

        return $this->json(
            $employee,
            200,
            [],
            [AbstractNormalizer::ATTRIBUTES => [
                'id',
                'firstName',
                'lastName',
                'email',
                'company' => ['id', 'name'],
                'roles'   => ['id', 'name']
            ]]
        );
    }


    /**
     * @Route("/aaa/{id}")
     */
    public function aaa($id, MailerInterface $mailer)
    {
        $employee = $this->getDoctrine()
            ->getRepository(Employee::class)
            ->find($id);


        $email = (new Email())
            ->from('hello@example.com')
            ->to($employee->getEmail())
            ->subject('Welcome')
            ->text('Sending emails is fun again!');

        $res = $mailer->send($email);

        return $this->json($res);
    }
}
