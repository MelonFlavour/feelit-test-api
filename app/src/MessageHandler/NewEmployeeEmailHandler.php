<?php

namespace App\MessageHandler;

use App\Message\NewEmployeeEmail;
use App\Repository\EmployeeRepository;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Mime\Email;

class NewEmployeeEmailHandler implements MessageHandlerInterface
{
    private $repository;
    private $mailer;

    public function __construct(EmployeeRepository $userRepository, MailerInterface $mailer)
    {
        $this->repository = $userRepository;
        $this->mailer     = $mailer;
    }

    public function __invoke(NewEmployeeEmail $email)
    {
        $employee = $this->repository->find($email->getEmployeeId());

        $email = (new Email())
            ->from('testapi@example.com')
            ->to($employee->getEmail())
            ->subject('New Employee subject')
            ->text('New Employee text');

        $this->mailer->send($email);
    }
}