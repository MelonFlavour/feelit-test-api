<?php


namespace App\Message;


class NewEmployeeEmail
{
    private $employeeId;

    public function __construct(int $employeeId)
    {
        $this->employeeId = $employeeId;
    }

    public function getEmployeeId(): int
    {
        return $this->employeeId;
    }
}