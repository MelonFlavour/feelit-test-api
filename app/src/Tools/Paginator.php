<?php


namespace App\Tools;

use Doctrine\Persistence\ObjectRepository;

class Paginator
{
    public function findByPage(ObjectRepository $repository, $page, $limit = 10)
    {
        return $repository->createQueryBuilder('t')
            ->orderBy('t.id', 'ASC')
            ->setMaxResults($limit)
            ->setFirstResult(($page - 1) * $limit)
            ->getQuery()
            ->getResult();
    }

}