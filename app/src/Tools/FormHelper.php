<?php

namespace App\Tools;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\Security\Csrf\CsrfToken;

class FormHelper
{
    /**
     * @param FormInterface $form
     * @return array
     */
    public function getErrorMessages(FormInterface $form)
    {
        $errors = [];

        foreach ($form->getErrors(true, true) as $error) {
            $errors[$error->getCause()->getPropertyPath()] = $error->getMessage();
        }

        return $errors;
    }
}