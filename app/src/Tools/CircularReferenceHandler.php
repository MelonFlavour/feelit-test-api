<?php


namespace App\Tools;


class CircularReferenceHandler
{
    public function __invoke($object)
    {
        return $object->getId();
    }
}