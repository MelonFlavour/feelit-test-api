# README #

### Setup ###

* Start docker: Navigate to project folder
```bash
$ docker-compose up -d --build
```
* Sign in to the php container
```bash
$ docker exec -it feelit-php74 /bin/bash
```
* Install composer packages
```bash
$ composer install
```
* Create database 
```bash
$ php bin/console doctrine:database:create
```
* Execute DB migrations
```bash
$ php bin/console doctrine:migrations:migrate
```

### How to use ###
You can use [Postman](https://www.postman.com/) application
In the repository you can find `FeelIt-Test.postman_collection.json` file. Import it to you Postman and you will see `FeelIt-Test` collection and requests inside it.

### ToDo ###
#### Improve validation ####
* Move validation constraints from entities to forms
* Make forms validate child objects 
* Get rid of `app/src/Tools/FormHelper.php` 
#### Improve output ####
* Try to get rid of the following:
```php
// src/Controller/CompanyController.php
[AbstractNormalizer::IGNORED_ATTRIBUTES => ['employees']
```
#### Improve pagination ####
* Create full-featured pagination:
* * count all the items
* * display current page
* * allow requesting amount of items per page
* * Get rid of `app/src/Tools/Paginator.php`